<?php
	require_once 'config/dbconf.php';
	require_once 'Db.php';
?>

<html>
	<body>
		<form action="" method="post">
			<label>Please paste your URL here: </label>
			<input name="url" type="url" value="<?= ( isset($_POST["url"]) ? $_POST["url"] : '' ) ?>"/>
			<input name="submit" type="submit" />
		</form>
		<div>
			<?php				
				if( isset( $_POST["url"] ) ) {
					// disable notices: if the pages is refreshed, and the form resubmits, thinking that it's empty when it isn't,
					// then this suppresses the resulting NOTICE that doesn't actually affect the operation of the rest of the code.
					error_reporting(E_ERROR | E_WARNING | E_PARSE);
				
					// full URL of the current page
					$url_base = ( isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://' ).$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
					$shorturl = new Db( CONNECTION_STRING.DB_NAME,DB_USER,DB_PASS );
					
					$already_exists = $shorturl->selectByFullPath( $_POST["url"] );
					if( $already_exists["record"] !== null && $already_exists["record"] !== false ) {
						echo '<p>Your shortened URL is: <a href="'.$url_base.$already_exists["url_short"].'">'.$url_base.$already_exists["url_short"].'</a></p>';
					} else {
						$insert_result = $shorturl->insert( $_POST["url"] );	
						echo '<p>Your shortened URL is: <a href="'.$url_base.$insert_result['url_short'].'">'.$url_base.$insert_result['url_short'].'</a></p>';
					}
				}
			?>
		</div>
	</body>
</html>