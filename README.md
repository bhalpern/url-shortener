## URL Shortener

1. **index.php** - Contains the form for submitting URLs to be shortened, including the submission code.
2. **redirect.php** - Handles all shortened URLs: Valid shortened URLs go to their URLs as listed in the database; Invalid URLs redirect to the root path (which should be **index.php**).
3. **.htaccess** - Detects incoming request paths, and, if they are at least 4 letters long (aka "http://short.local/xyz1"), then they are handled by **redirect.php**.
4. **Db.php** - This is a class, wrapping the database connection. All database operations, including deriving the relevant shortened URL go here.
5. **shorturl.sql** - A dump file containing sample data from the database.

---

## Notes

1. It was interesting to learn, in the course of this project, that it is better to derive short strings representing long strings from their unique ID in the database, instead of from a hash or other compression method, in order to guarantee that collisions would be impossible.

2. To that end, in order to keep the shortened URLs from being 1-2 characters, or revealing their ID in the database, I had the primary key auto_increment from 10000, instead of from 1.

3. There is a config file that is not included in this repository. It contains the following constants: CONNECTION_STRING, DB_NAME, DB_USER, DB_PASS --which are to be used for establishing the database connection. See **.gitignore** for details.

4. The database table has a UNIQUE constraint on the "full_url" column to prevent duplicates.

5. The system no longer regards "https://www.google.com" and "https://www.google.com/" (note the trailing "/") as two distinct URLs.