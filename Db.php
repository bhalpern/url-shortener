<?php

class Db {
	
	/*
	 * Even though the ID of a record in the database is always a base10 integer, I want my shortened URLs to be longer than 3 characters for stylistic reasons.
	 * Therefore, within this class, I will treat them as if they are really in base23 (because prime numbers are awesome), and convert them to base36 (alphabet+[0-9]).
	 *
	 */
	const ID_BASE = 23;
	const SHORT_BASE = 36;
	
	private $_connection = false;
	
	public function __construct( $conn_string, $user, $pass ) {
		$this->_connection = new PDO($conn_string,$user,$pass);
	}

	public function insert( $url ) {
		$insert = $this->_connection->prepare('INSERT INTO shorturl( full_url ) VALUES( :url )');
		$success = $insert->execute( array( ':url' => rtrim($url, "/") ) );
		return array( 'success' => $success, 
					  'error_info' => $insert->errorInfo(), 
					  'url_short' => ( $success ? base_convert($this->_connection->lastInsertId(), self::ID_BASE, self::SHORT_BASE ) : null ) );
	}
	
	public function selectByShortURL( $short_str ) {
		$select = $this->_connection->prepare('SELECT * FROM shorturl WHERE id = :id');
		$success = $select->execute( array( ':id' => base_convert($short_str, self::SHORT_BASE, self::ID_BASE ) ) );
		return array( 'success' => $success, 
					  'error_info' => $select->errorInfo(), 
					  'record' => $select->fetch(PDO::FETCH_OBJ) );
	}
	
	public function selectByFullPath( $url ) {
		$select = $this->_connection->prepare('SELECT * FROM shorturl WHERE full_url = :url');
		$success = $select->execute( array( ':url' => rtrim($url, "/") ) );
		$record = ( $success ? $select->fetch(PDO::FETCH_OBJ) : null );
		return array( 'success' => $success, 
					  'error_info' => $select->errorInfo(), 
					  'record' =>  $record,
					  'url_short' => ( $success ? base_convert($record->id, self::ID_BASE, self::SHORT_BASE ) : null ) ) ;
	}
}