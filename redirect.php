<?php

require_once 'config/dbconf.php';
require_once 'Db.php';

// in case of bad input, be able to redirect back to the current page
$current_page = rtrim( ( isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://' ).$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], "/");
$current_page = substr($current_page, 0, strrpos($current_page, "/"));

// goto homepage if this page is accessed incorrectly
// .htaccess regex prevents this from being triggered.
if( ! isset( $_GET['shorturl'] ) || strlen($_GET['shorturl']) < 4 ) {
	header('Location: '.$current_page);
	die();
}

// goto homepage if an invalid URL is provided
$db = new Db(CONNECTION_STRING.DB_NAME,DB_USER,DB_PASS);
$destination = $db->selectByShortURL( $_GET['shorturl'] )["record"]->full_url;
if( ! $destination ) {
	header('Location: '.$current_page);	
	die();
}
// goto valid URL
header('Location: '.$destination);
die();

