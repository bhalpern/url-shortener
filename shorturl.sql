CREATE TABLE `shorturl` (
  `id` int(11) NOT NULL,
  `full_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shorturl`
--

INSERT INTO `shorturl` (`id`, `full_url`) VALUES(10003, 'https://www.xkcd.com');
INSERT INTO `shorturl` (`id`, `full_url`) VALUES(10005, 'https://www.facebook.com');
INSERT INTO `shorturl` (`id`, `full_url`) VALUES(10008, 'https://www.fbook.com');
INSERT INTO `shorturl` (`id`, `full_url`) VALUES(10013, 'https://www.f2book.com');
INSERT INTO `shorturl` (`id`, `full_url`) VALUES(10034, 'https://www.google.com');
INSERT INTO `shorturl` (`id`, `full_url`) VALUES(10035, 'https://facebook.com');
INSERT INTO `shorturl` (`id`, `full_url`) VALUES(10036, 'http://www.reddit.com');



--
-- Indexes for dumped tables
--

--
-- Indexes for table `shorturl`
--
ALTER TABLE `shorturl`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `full_url` (`full_url`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shorturl`
--
ALTER TABLE `shorturl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10037;
COMMIT;
